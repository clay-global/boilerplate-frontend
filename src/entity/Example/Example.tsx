import { Section } from "@/feature/Section";
import { Container } from "@/feature/Container";
import { Image } from "@/feature/Image";
import { Button } from "@/feature/Button";
import { Link } from "@/feature/Link";
import { PageData, ImageData } from "@/api/interface";

interface ExampleData extends PageData {
  example_title: string | null;
  example_image: ImageData | null;
}

interface ExampleAdditionalData {}

export interface Example {
  (props: {
    page: ExampleData;
    data: ExampleAdditionalData;
  }): React.ReactElement;
}

export const Example: Example = ({ page, data }) => {
  return (
    <Section>
      <Container>
        <h2>{page.example_title || "Title"}</h2>
        <Link>Link</Link>
        <Image
          src={
            page.example_image?.url ||
            "https://upload.wikimedia.org/wikipedia/ru/7/7e/Hellokitty_b1.png"
          }
          width="96"
          height="75"
          alt=""
        />
        <Button>Button</Button>
      </Container>
    </Section>
  );
};
