import Head from "next/head";
import { Header } from "@/entity/Header";
import { Footer } from "@/entity/Footer";
import { PageData, AdditionalPageData } from "@/api/interface";
import { Meta } from "./Meta";

export interface Layout {
  (props: {
    page: PageData;
    data: AdditionalPageData;
    children: React.ReactNode;
  }): React.ReactElement;
}

export const Layout: Layout = ({ page, data, children }) => {
  return (
    <>
      <Head>
        <Meta 
          settings={data.settings}
          uri={page.uri}
          title={page.title}
          description={page.description}/>
      </Head>

      <Header
        page={page as Parameters<Header>[0]["page"]}
        data={data as Parameters<Header>[0]["data"]}
      />
      <div className="root">{children}</div>
      <Footer
        page={page as Parameters<Header>[0]["page"]}
        data={data as Parameters<Header>[0]["data"]}
      />
    </>
  )
}
