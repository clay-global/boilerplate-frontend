// Styles
const classNames = require("classnames/bind");
import styles from "./Footer.module.scss";
const cx = classNames.bind(styles);
import { PageData } from "@/api/interface";

interface FooterData extends PageData {}

interface FooterAdditionalData {}

export interface Footer {
  (props: {
    page: FooterData;
    data: FooterAdditionalData;
    className?: string;
  }): React.ReactElement;
}

export const Footer: Footer = ({ page, data, className }) => {
  return <footer className={cx("Footer", className)}></footer>;
};
