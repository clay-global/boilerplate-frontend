module.exports = {
  reactStrictMode: true,
  images: {},
  env: {
    API_URL: process.env.API_URL
  },
  swcMinify: false,
}
