import React from "react";
import { SettingsData } from "@/api/requests/getSettings";

interface Meta {
  (props: {
    settings: SettingsData;
    title: string;
    description: string;
    uri: string;
  }): React.ReactElement;
}

declare global {
  interface Window { dataLayer: any; }
}

export const Meta: Meta = ({ settings, title, description, uri }) => {
  const metaTitle = `${
    settings?.meta_title_prefix || ""
  } - ${title}`;

  const metaDescription =
    description || settings?.meta_description_default || "";

  const metaImage =
    settings && settings.meta_social_image
      ? settings.meta_social_image.permalink
      : "/Social.jpg";

  const metaImageWidth =
    settings && settings.meta_social_image
      ? settings.meta_social_image.width
      : 1200;

  const metaImageHeight =
    settings && settings.meta_social_image
      ? settings.meta_social_image.height
      : 630;

  const origin = (process.env.BASE_URL as string).replace(/\/$/, "");

  const injectGA = (id: string) => {
    if (typeof window === "undefined") {
      return;
    }

    window.dataLayer = window.dataLayer || [];

    function gtag(type: string, id: Date | string) {
      window.dataLayer.push(arguments);
    }

    gtag("js", new Date());
    gtag("config", id);

    return <></>;
  };

  return <>
            <meta charSet="utf-8" />
          <meta name="theme-color" content="#E3FDFD" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta property="og:locale" content="en_EN" />
          <meta property="og:title" content={metaTitle} />
          <meta property="og:url" content={origin + (uri || "")} />
          <meta property="og:type" content="website" />
          <meta property="og:description" content={metaDescription} />
          <meta property="og:site_name" content={metaTitle} />
          <meta property="og:image" content={metaImage} />
          <meta property="og:image:alt" content={metaTitle} />
          <meta property="og:image:width" content={`${metaImageWidth}`} />
          <meta property="og:image:height" content={`${metaImageHeight}`} />
          <meta property="og:image:url" content={metaImage} />
          <meta property="og:image:secure_url" content={metaImage} />
          <meta name="twitter:card" content="summary" />
          <meta name="twitter:title" content={metaTitle} />
          <meta name="twitter:description" content={metaDescription} />
          <meta name="twitter:image" content={metaImage} />
          <link rel="image_src" href={metaImage} />

          <title>{metaTitle}</title>
          <meta name="description" content={metaDescription} />

          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon-16x16.png"
          />
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
          <meta name="msapplication-TileColor" content="#da532c" />
          <meta name="theme-color" content="#ffffff"></meta>

          {settings && settings.google_analytics_id && 
            <>
              <script async src={`https://www.googletagmanager.com/gtag/js?id=${settings.google_analytics_id}`}></script>
              <script>{injectGA(settings.google_analytics_id)}</script>
            </>
          }</>;
};
