import { syncScroll } from "./syncScroll";
import { useScrollLock } from "./useScrollLock";
import { useScrollTrigger } from "./useScrollTrigger";

export {
  syncScroll,
  useScrollLock,
  useScrollTrigger
}