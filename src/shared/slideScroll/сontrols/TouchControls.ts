import { slideScrollController } from "../useSlideScroll";
import { SlideScrollAnimation } from "../animations";

export class TouchControls {
  private id: string;
  private container: React.RefObject<HTMLDivElement | null>;
  private containerHeight: number = 0;
  private touchStartData = {
    x: 0,
    y: 0,
    time: 0,
    index: 0,
    total: 0
  }
  private touchDelta = {
    x: 0,
    y: 0
  }
  private isHorizontalMovement: boolean | undefined;
  private isTouchStarted = false;
  private isOnEdge = false;
  private animationModule: SlideScrollAnimation;

  constructor (id: string, container: React.RefObject<HTMLDivElement | null>, animationModule: SlideScrollAnimation) {
    this.id = id;
    this.container = container;
    this.animationModule = animationModule;

    if (this.container.current) {
      this.container.current.addEventListener("touchstart", this.handleTouchstart);
      this.container.current.addEventListener("touchmove", this.handleTouchmove);
      this.container.current.addEventListener("touchend", this.handleTouchend);
    }
  }

  destructor = () => {
    if (this.container.current) {
      this.container.current.removeEventListener("touchstart", this.handleTouchstart);
      this.container.current.removeEventListener("touchmove", this.handleTouchmove);
      this.container.current.removeEventListener("touchend", this.handleTouchend);
    }
  }

  handleTouchstart = (e:TouchEvent) => {
		const touches = e.touches[0];
    const state = slideScrollController.getStateById(this.id);

    if (!state) return;
    if (!this.container.current) return;

		this.containerHeight = this.container.current.clientHeight;

		this.touchStartData = {
			x: touches.pageX,
			y: touches.pageY,
			time: new Date().getTime(),
      index: state.index,
      total: state.total
		}

		this.touchDelta = {
      x: 0,
      y: 0
    }

		this.isHorizontalMovement = undefined;
    this.isTouchStarted = true;
  }
  
  handleTouchmove = (e:TouchEvent) => {
		if (e.touches.length > 1) return;
    if (!this.isTouchStarted) return;
		const touches = e.touches[0];

		this.touchDelta = {
			x: touches.pageX - this.touchStartData.x,
			y: touches.pageY - this.touchStartData.y,
		};

		if (typeof this.isHorizontalMovement === undefined) {
			this.isHorizontalMovement = !!(
				this.isHorizontalMovement ||
				Math.abs(this.touchDelta.y) < Math.abs(this.touchDelta.x)
			);
		}

		if (this.isHorizontalMovement) return;

		let touchShift = this.touchDelta.y / 6;

		if (this.touchStartData.index || this.touchStartData.total) {
			if (this.touchDelta.y > 0 && this.touchStartData.index <= 0) {
				this.isOnEdge = true;
			} else if (this.touchDelta.y < 0 && this.touchStartData.index >= this.touchStartData.total - 1) {
				this.isOnEdge = true;
			} else {
				this.isOnEdge = false;
			}
		} else {
			this.isOnEdge = false;
		}

		if (this.isOnEdge) {
			touchShift /= 4;
		}

    this.animationModule.touchShift(this.touchStartData.index, touchShift);
  }
  
  handleTouchend = (e:TouchEvent) => {
    if (!this.isTouchStarted) return;
    this.isTouchStarted = false;

		const duration = new Date().getTime() - this.touchStartData.time;
		const check =
			(duration < 250 && Math.abs(this.touchDelta.y) > 20) ||
			Math.abs(this.touchDelta.y) > 100;

		if (this.isHorizontalMovement) return;

		if (check && !this.isOnEdge) {
      if (this.touchDelta.y > 0) {
        slideScrollController.slideUp(this.id);
      } else {
        slideScrollController.slideDown(this.id);
      }
      this.animationModule.touchShiftCancel(1);
		} else {
			if (this.isOnEdge) {
        this.animationModule.touchShiftCancel(0.5);
      } else {
        this.animationModule.touchShiftCancel(1);
      }
		}
  }
}