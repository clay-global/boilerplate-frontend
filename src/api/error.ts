export const requestError = (route: unknown, err: unknown) => {
  return `Request to ${route} falied. Check ".env.development" and ".env.production" files and then run "npm run build" command. ${err}`;
}