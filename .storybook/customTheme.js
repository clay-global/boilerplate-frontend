import { create } from '@storybook/theming';

export default create({
  brandTitle: 'Clay / Shared Library',

  base: 'light',

  colorPrimary: '#0a0a0a',
  colorSecondary: '#0a0a0a',

});