import { Carousel } from "./CarouselMain";
import { useCarousel, carouselController } from "./useCarousel";
import { CarouselControl } from "./CarouselControl";

export { Carousel, CarouselControl, useCarousel, carouselController };
