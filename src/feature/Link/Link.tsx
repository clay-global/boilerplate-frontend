import { forwardRef, HTMLProps } from "react";
import classNames from "classnames";
import { default as NextLink } from "next/link";

import styles from "./Link.module.scss";

export interface LinkProps extends HTMLProps<HTMLAnchorElement> {
  /** Link appearence variant. Put your own variant here */
  variant?: "base" | "button";
}

export const Link = forwardRef<HTMLAnchorElement, LinkProps>(
  (
    { className, variant = "base", href = "#", target, children, ...restProps },
    ref
  ) => {
    return (
      <NextLink href={href}>
        <a
          className={classNames(
            styles.Link,
            variant === "button" && styles.Link_Button,
            className
          )}
          href={href}
          target={target}
          ref={ref}
          {...restProps}
        >
          {children}
        </a>
      </NextLink>
    );
  }
);

Link.displayName = "Link";
