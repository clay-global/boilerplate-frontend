import { ReactElement, useCallback, useRef, useEffect, useState } from "react";

import { modalController } from "./Modal/modalController";
import { useAlwaysFocusable } from "../focus/useAlwaysFocusable";

type ControlActions = "toggle" | "open" | "close" | "close-all" | "toggle-all";

interface ModalControl {
  (props: {
    /**
     * Unique modal
     */
    id: string;
    /**
     * Action that should be fired on click: "toggle" | "open" | "close" | "close-all" | "toggle-all"
     */
    action?: ControlActions;
    /**
     * Classname of the active control
     */
    activeClassName?: string;
    /**
     * If this control should alwayes be focusable
     */
    alwaysFocusable?: boolean;
    ariaLabel?: string;
    className?: string;
    style?: React.CSSProperties;
    children?: React.ReactNode;
    tabIndex?: number;
    onClick?: () => void;
  }): ReactElement<HTMLButtonElement>;
}

export const ModalControl: ModalControl = ({
  id,
  action = "toggle",
  className,
  style,
  children,
  activeClassName = "active",
  alwaysFocusable = false,
  ariaLabel,
  tabIndex,
  onClick,
}) => {
  const [active, setActive] = useState(
    modalController.getState().activeModal === id
  );

  const wrapperElement = useRef<HTMLButtonElement | null>(null);

  useAlwaysFocusable(alwaysFocusable ? wrapperElement : undefined);

  const handleClick = useCallback(() => {
    if (onClick) {
      onClick();
    }

    if (action === "open") {
      modalController.open(id);
    } else if (action === "close") {
      modalController.close(id);
    } else if (action === "toggle") {
      modalController.toggle(id);
    } else if (action === "close-all") {
      modalController.closeAll();
    }
  }, [id, action]);

  const classes: string[] = [];
  if (action === "open" || action === "close" || action === "toggle") {
    if (active) {
      classes.push(activeClassName);
    }
  } else if (action === "close-all" || action === "toggle-all") {
    if (active) {
      classes.push(activeClassName);
    }
  }

  if (className) {
    classes.push(className);
  }

  useEffect(() => {
    if (wrapperElement.current) {
      wrapperElement.current.setAttribute(
        "aria-label",
        ariaLabel || "Open modal"
      );
    }
  }, [ariaLabel]);

  const handleState = useCallback(() => {
    if (!wrapperElement.current) return;

    if (modalController.getState().activeModal === id) {
      setActive(true);
    } else {
      setActive(false);
    }
  }, [id]);

  useEffect(() => {
    modalController.subscribe(handleState);
    return () => {
      modalController.unsubscribe(handleState);
    };
  }, [handleState]);

  return (
    <button
      ref={wrapperElement}
      className={classes.join(" ")}
      onClick={handleClick}
      style={style}
      aria-haspopup={
        action !== "close" && action !== "close-all" ? "true" : undefined
      }
      aria-expanded={
        action !== "close" && action !== "close-all" ? active : undefined
      }
      aria-label={ariaLabel}
      tabIndex={tabIndex}
    >
      {children}
    </button>
  );
};
