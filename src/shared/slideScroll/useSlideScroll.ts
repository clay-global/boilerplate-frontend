import { useCallback, useEffect, useMemo, useState } from "react";
import { EventEmitter, Event } from "../utils/EventEmitter";
import { objectsEqual } from "../utils/object.utils";

type SlideScrollEventTypes =
  | "slideScroll:add"
  | "slideScroll:remove"
  | "slideScroll:set"
  | "slideScroll:reset"
  | "slideScroll:toIndex"
  | "slideScroll:toDirection";

interface PrivateEvent extends Event {
  type: SlideScrollEventTypes;
  payload: {
    id: string;
    item?: {
      index?: number;
      total?: number;
      lockDuration?: number;
      animationDuration?: number;
      lockScroll?: boolean;
      enabled?: boolean;
      isLocked?: boolean;
      isNativeScroll?: boolean;
    };
    direction?: "up" | "down";
  };
}

export const privateEventEmitter = new EventEmitter<PrivateEvent>();

export interface SlideScrollEvent extends Event {
  type: "update";
  payload?: undefined;
}

const slideScrollEventEmitter = new EventEmitter<SlideScrollEvent>();

interface SlideScrollItem {
  id: string;
  index: number;
  total: number;
  isNativeScroll: boolean;
  isDisabled: boolean;
  isLocked: boolean;
  lockDuration: number;
  animationDuration: number;
  private: {
    defaultAnimationDuration: number;
    defaultLockDuration: number;
    toReset: boolean;
  };
}

interface SlideScrollState {
  [key: string]: SlideScrollItem | undefined;
}

/**
 * Object representing the global state of all SlideScroll components. Can be used outside of the React components
 */
const slideScrollGlobalState: SlideScrollState = {};

privateEventEmitter.subscribe((e: PrivateEvent) => {
  if (e.type === "slideScroll:add") {
    if (slideScrollGlobalState[e.payload.id]) {
      throw new Error(`Slide scroll with id: ${e.payload.id} already exists`);
    }

    slideScrollGlobalState[e.payload.id] = {
      id: e.payload.id,
      index: e.payload.item?.index || 0,
      total: e.payload.item?.total || 0,
      isNativeScroll: false,
      isLocked: false,
      isDisabled: false,
      lockDuration: e.payload.item?.lockDuration || 0.6,
      animationDuration: e.payload.item?.animationDuration || 0.6,
      private: {
        defaultAnimationDuration: e.payload.item?.animationDuration || 0.6,
        defaultLockDuration: e.payload.item?.lockDuration || 0.6,
        toReset: false,
      },
    };

    slideScrollEventEmitter.dispatch({
      type: "update",
    });
  } else if (e.type === "slideScroll:remove") {
    if (!slideScrollGlobalState[e.payload.id]) {
      throw new Error(`Slide scroll with id: ${e.payload.id} doesnt exist`);
    }

    delete slideScrollGlobalState[e.payload.id];

    slideScrollEventEmitter.dispatch({
      type: "update",
    });
  } else if (e.type === "slideScroll:set") {
    const slideScrollItem = slideScrollGlobalState[e.payload.id];

    if (slideScrollItem === undefined) {
      throw new Error(`Slide scroll with id: ${e.payload.id} doesnt exist`);
    }

    const previousItem = { ...slideScrollItem };

    if (e.payload.item && e.payload.item.index !== undefined) {
      slideScrollItem.index = e.payload.item.index;
    }

    slideScrollGlobalState[e.payload.id] = {
      ...slideScrollItem,
      ...e.payload.item,
    };

    if (!objectsEqual(previousItem, slideScrollGlobalState[e.payload.id])) {
      slideScrollEventEmitter.dispatch({
        type: "update",
      });
    }
  } else if (e.type === "slideScroll:reset") {
    const slideScrollItem = slideScrollGlobalState[e.payload.id];

    if (slideScrollItem === undefined) {
      throw new Error(`Slide scroll with id: ${e.payload.id} doesnt exist`);
    }

    slideScrollItem.animationDuration =
      slideScrollItem.private.defaultAnimationDuration;
    slideScrollItem.lockDuration = slideScrollItem.private.defaultLockDuration;
  } else if (e.type === "slideScroll:toIndex") {
    const slideScrollItem = slideScrollGlobalState[e.payload.id];

    if (slideScrollItem === undefined) {
      throw new Error(`Slide scroll with id: ${e.payload.id} doesnt exist`);
    }

    if (slideScrollItem.isLocked) return;

    if (
      !e.payload.item ||
      e.payload.item.index === undefined ||
      slideScrollItem.index === e.payload.item.index
    ) {
      return;
    }

    slideScrollItem.index = e.payload.item.index;

    slideScrollEventEmitter.dispatch({
      type: "update",
    });

    slideScrollItem.isLocked = true;

    setTimeout(() => {
      slideScrollItem.isLocked = false;
    }, slideScrollItem.lockDuration * 1000);
  } else if (e.type === "slideScroll:toDirection") {
    const slideScrollItem = slideScrollGlobalState[e.payload.id];

    if (slideScrollItem === undefined) {
      throw new Error(`Slide scroll with id: ${e.payload.id} doesnt exist`);
    }

    if (slideScrollItem.isLocked) return;

    if (e.payload.direction === "up") {
      if (slideScrollItem.index === 0) {
        return;
      }
      slideScrollItem.index--;
    } else if (e.payload.direction === "down") {
      if (slideScrollItem.index === slideScrollItem.total - 1) {
        return;
      }
      slideScrollItem.index++;
    }

    slideScrollEventEmitter.dispatch({
      type: "update",
    });

    slideScrollItem.isLocked = true;
    setTimeout(() => {
      slideScrollItem.isLocked = false;
    }, slideScrollItem.lockDuration * 1000);
  }
});

export const slideScrollController = {
  slideUp: (id: string) => {
    privateEventEmitter.dispatch({
      type: "slideScroll:toDirection",
      payload: {
        id,
        direction: "up",
      },
    });
  },

  slideDown: (id: string) => {
    privateEventEmitter.dispatch({
      type: "slideScroll:toDirection",
      payload: {
        id,
        direction: "down",
      },
    });
  },

  slideTo: (id: string, index: number) => {
    privateEventEmitter.dispatch({
      type: "slideScroll:toDirection",
      payload: {
        id,
        item: {
          index,
        },
      },
    });
  },

  getAllStates: () => {
    return slideScrollGlobalState;
  },

  getStateById: (id: string) => {
    return slideScrollGlobalState[id];
  },

  subscribe: (handler: (e: SlideScrollEvent) => void) => {
    slideScrollEventEmitter.subscribe(handler);
  },

  unsubscribe: (handler: (e: SlideScrollEvent) => void) => {
    slideScrollEventEmitter.unsubscribe(handler);
  },
};

export const useSlideScroll = () => {
  const [localState, setLocalState] = useState<SlideScrollState>(
    slideScrollGlobalState
  );

  const handleEventEmitter = useCallback((e: SlideScrollEvent) => {
    if (e.type === "update") {
      setLocalState({ ...slideScrollGlobalState });
    }
  }, []);

  const slideUp = useCallback((id: string) => {
    slideScrollController.slideUp(id);
  }, []);

  const slideDown = useCallback((id: string) => {
    slideScrollController.slideDown(id);
  }, []);

  const slideTo = useCallback((id: string, index: number) => {
    slideScrollController.slideTo(id, index);
  }, []);

  useEffect(() => {
    slideScrollEventEmitter.subscribe(handleEventEmitter);
    return () => {
      slideScrollEventEmitter.unsubscribe(handleEventEmitter);
    };
  }, []);

  return useMemo(() => {
    return {
      state: localState,
      slideUp,
      slideDown,
      slideTo,
    };
  }, [localState, slideUp, slideDown, slideTo]);
};
