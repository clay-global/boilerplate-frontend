import { Modal, modalController, useModal } from "./Modal";
import { ModalControl } from "./ModalControl";

export { Modal, ModalControl, modalController, useModal};
