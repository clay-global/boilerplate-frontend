import { Modal } from "./Modal";
import { modalController, useModal } from "./modalController";

export {
  Modal,
  modalController,
  useModal
}