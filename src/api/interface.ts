export interface ImageData {
  id: string;
  url: string;
  alt: string | null;
  height: number;
  width: number;
  permalink: string;
}

export interface PageShortData {
  blueprint: {
    handle: string;
    title: string;
  };
  date: string;
  id: string;
  last_modified: string;
  locale: string;
  parent: {
    id: string;
    title: string;
    api_url: string;
  };
  private: boolean;
  published: boolean;
  slug: string;
  status: string;
  title: string;
  updated_at: string;
  uri: string;
  url: string;
}

export interface PageData extends PageShortData {
  [index: string]: any;
}

export interface AdditionalPageData {
  [index: string]: any;
}

export interface PageProps {
  page: PageData | null;
  type: string;
  data: AdditionalPageData;
}
