export const objectsEqual = (
  firstObject: object | null | undefined,
  secondObject: object | null | undefined
): boolean => {
  if (firstObject === secondObject) return true;
  if (firstObject === null || secondObject === null) return false;
  if (firstObject === undefined || secondObject === undefined) return false;
  if (Object.keys(firstObject).length !== Object.keys(secondObject).length)
    return false;

  return Object.keys(firstObject).every(
    (key) =>
      (firstObject as { [index: string]: any })[key] ===
      (secondObject as { [index: string]: any })[key]
  );
};
