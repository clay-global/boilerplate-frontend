import { carouselController } from "../../useCarousel";

export interface FocusControls {
  destroy: VoidFunction;
}

interface FocusControlBasicsOptions {
  ariaLabel: string;
}

interface FocusControlsOptions extends FocusControlBasicsOptions {
  container: HTMLElement;
  id: string;
}

export class FocusControls {
  private container;
  private id;
  private isFocusOn = false;

  constructor(options: FocusControlsOptions) {
    this.id = options.id;
    this.container = options.container;

    this.container.setAttribute("tabindex", "0");
    this.container.setAttribute("aria-label", options.ariaLabel);

    this.container.addEventListener("focus", this.handleFocus);
    this.container.addEventListener("blur", this.handleBlur);
    document.addEventListener("keydown", this.handleKeyboard);
  }

  update = (options: FocusControlBasicsOptions) => {
    this.container.setAttribute("aria-label", options.ariaLabel);
  }

  destroy = () => {
    this.container.removeAttribute("tabindex");
    this.container.removeAttribute("aria-label");

    this.container.removeEventListener("focus", this.handleFocus);
    this.container.removeEventListener("blur", this.handleBlur);
    document.removeEventListener("keydown", this.handleKeyboard);
  };

  private handleFocus = () => {
    this.isFocusOn = true;
  };

  private handleBlur = () => {
    this.isFocusOn = false;
  };

  private handleKeyboard = (e: KeyboardEvent) => {
    if (!this.isFocusOn) return;

    if (e.key === "ArrowLeft") {
      carouselController.byIndex(this.id, -1);
    } else if (e.key === "ArrowRight") {
      carouselController.byIndex(this.id, 1);
    }
  };
}
