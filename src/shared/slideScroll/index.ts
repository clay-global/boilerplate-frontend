import { SlideScroll } from "./SlideScroll";
import { useSlideScroll, slideScrollController, SlideScrollEvent} from "./useSlideScroll";

export {
  SlideScroll,
  useSlideScroll,
  slideScrollController,
}

export type {
  SlideScrollEvent
}