export interface SlideScrollAnimationOptions {
  slideRefs: Array<React.RefObject<HTMLElement | null>>;
  container: React.RefObject<HTMLDivElement | null>;
}

export interface SlideScrollAnimation {
  init: (currentIndex: number) => void,
  animate: (previousIndex: number, currentIndex: number, duration: number) => void,
  touchShift: (currentIndex: number, shift: number) => void,
  touchShiftCancel: (duration: number) => void,
}