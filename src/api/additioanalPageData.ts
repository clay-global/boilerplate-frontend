import { getMenu } from "./requests/getMenu";
import { getSettings } from "./requests/getSettings";
// import { getBrands } from "./requests/getBrands";

interface AdditionalRequests {
  [index: string] : () => Promise<any>
}

interface AdditionalData {
  [index: string]: AdditionalRequests
}

export const additionalDataDefault: AdditionalRequests = {
  menu: getMenu,
  settings: getSettings
}

export const additionalDataByType: AdditionalData = {
  // home: {
  //   brands: getBrands,
  // },
}