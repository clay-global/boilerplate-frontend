import React, {
  useCallback,
  useEffect,
  useRef,
  useState,
  ReactElement,
  CSSProperties,
} from "react";
import { modalController, _modalControllerPrivate } from "./modalController";
import { useRouter } from "next/dist/client/router";
import { useScrollLock } from "../../scroll";
import { getFocusable } from "../../utils/dom.utils";
import { useFocusLock } from "../../focus";
import { OffsetScrollbar } from "../../scroll/types";
import classNames from "classnames";

export interface Modal {
  (props: {
    /**
     * Unique modal Id. Required.
     */
    id: string;
    /**
     * Id of the group of the modals. (Modal component can be used without the ModalGroup wrapper, but scroll locking will not work)
     */
    groupId?: string;
    /**
     * Classname that will be added when the modal window is active.
     */
    activeClassName?: string;
    /**
     * Classname that will be added when the modal window is initialized.
     */
    initializedClassName?: string;
    /**
     * If focus should be locked inside of the modal window. Check "focus/useAlwaysFocusable" to add elements that shoul always be focusable
     */
    focusLock?: boolean;
    /**
     * If modal should be initailly open
     */
    opened?: boolean;
    /**
     * If modal should close on router push event
     */
    closeOnRoute?: boolean;
    /**
     * Property that should be used to offset the scrollbar on windows. Default is "paddingRight"
     */
    offsetScrollbar?: OffsetScrollbar;

    className?: string;
    style?: React.CSSProperties;
    children?: React.ReactNode;
  }): ReactElement<HTMLDivElement>;
}

export const Modal: Modal = ({
  id,
  className,
  activeClassName = "active",
  initializedClassName = "initialized",
  style = {},
  children = null,
  opened = false,
  closeOnRoute = true,
  focusLock = true,
  offsetScrollbar = "paddingRight",
}) => {
  const [initialized, setInitialized] = useState<boolean>(false);
  const [active, setActive] = useState<boolean>(
    modalController.getState().activeModal === id
  );

  const activeRef = useRef(false);
  const wrapperElement = useRef<HTMLDivElement | null>(null);
  const focusLockHook = useFocusLock(wrapperElement);

  const lockFocus = focusLockHook.lock;
  const unlockFocus = focusLockHook.unlock;
  const disabledElementsRef = useRef<HTMLElement[]>([]);

  const enableFocus = useCallback(() => {
    if (!wrapperElement.current) return;
    disabledElementsRef.current.forEach((el) => {
      el.setAttribute("tabindex", "0");
    });

    disabledElementsRef.current = [];

    if (focusLock) {
      lockFocus();
    }
  }, [focusLock, lockFocus]);

  const disableFocus = useCallback(() => {
    if (!wrapperElement.current) return;

    getFocusable(wrapperElement.current).forEach((el) => {
      disabledElementsRef.current.push(el);
      el.setAttribute("tabindex", "-1");
    });

    if (focusLock) {
      unlockFocus();
    }
  }, [focusLock, unlockFocus]);

  useEffect(() => {
    _modalControllerPrivate.addModal({
      id,
    });
    return () => {
      _modalControllerPrivate.removeModal(id);
    };
  }, []);

  const handleState = useCallback(() => {
    const stateActive = modalController.getState().activeModal === id;

    if (stateActive && !activeRef.current) {
      activeRef.current = true;

      enableFocus();
      setActive(true);
    } else if (!stateActive && activeRef.current) {
      activeRef.current = false;

      disableFocus();
      setActive(false);
    }

    if (wrapperElement.current) {
      if (stateActive) {
        disabledElementsRef.current.forEach((el) => {
          el.setAttribute("tabindex", "0");
        });

        disabledElementsRef.current = [];
      } else {
        getFocusable(wrapperElement.current).forEach((el) => {
          disabledElementsRef.current.push(el);
          el.setAttribute("tabindex", "-1");
        });
      }
    }
  }, [id]);

  useEffect(() => {
    setTimeout(() => {
      setInitialized(true);
    }, 1000 / 60);

    modalController.subscribe(handleState);

    return () => {
      modalController.unsubscribe(handleState);
    };
  }, [handleState]);

  const router = useRouter();

  useEffect(() => {
    const handleRouter = () => {
      modalController.close(id);
    };

    if (closeOnRoute) {
      router.events.on("routeChangeStart", handleRouter);
    }

    return () => {
      if (closeOnRoute) {
        router.events.off("routeChangeStart", handleRouter);
      }
    };
  }, [closeOnRoute, id, router.events]);

  useEffect(() => {
    if (opened) {
      modalController.open(id);
    }
  }, [opened, id]);

  const scrollLockHook = useScrollLock();

  if (offsetScrollbar === "right") {
    style.right = scrollLockHook.offset;
  } else if (offsetScrollbar === "paddingRight") {
    style.paddingRight = scrollLockHook.offset;
  } else if (offsetScrollbar === "marginRight") {
    style.marginRight = scrollLockHook.offset;
  }

  style.transitionDuration = initialized ? "" : "0s";

  const newStyle: CSSProperties = {
    pointerEvents: active ? "auto" : "none",
  };

  return (
    <div
      ref={wrapperElement}
      className={classNames(
        className,
        active && activeClassName,
        initialized && initializedClassName
      )}
      style={{ ...newStyle, ...style }}
    >
      {children}
    </div>
  );
};
