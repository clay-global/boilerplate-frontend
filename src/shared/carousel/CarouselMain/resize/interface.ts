export interface Resize {
  update: (options: ResizeBasicOptions) => void;
  destroy: VoidFunction;
  resize: () => {
    containerWidth: number,
    itemWidth: number,
    gapWidth: number,
    totalWidth: number,
    leftShift: number,
    numberOfFakeElements: number
  } | undefined;
}

export interface ResizeBasicOptions {
  resizeElements: boolean;
  gap: number;
  padding: number;
  paddingUnits: "px" | "%" | "vw";
  itemWidth: number;
  maximumItemWidth: number;
  total: number;
  elementsPerScreen: {
    breakpoint: number,
    number: number,
  }[];
}

export interface ResizeOptions extends ResizeBasicOptions {
  container: HTMLElement;
  outerWrapper: HTMLElement;
  mode: "simple" | "continuous";
}