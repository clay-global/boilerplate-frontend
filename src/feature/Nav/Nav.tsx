import { FC } from "react";
import classNames from "classnames";
import Link from "next/link";

import { MenuItem } from "@/api/requests/getMenu";
import { DropDown, dropDownAnimationList } from "@/shared/dropDown";

import styles from "./Nav.module.scss";

export interface NavProps {
  menu: MenuItem[];
  className?: string;
}

export const Nav: FC<NavProps> = ({ menu, className }) => {
  if (!menu) {
    return null;
  }

  const getLinkItem = ({ uri, title }: MenuItem["page"]) =>
    uri && (
      <Link href={uri}>
        <a role="menuitem" className={styles.Item}>
          {title}
        </a>
      </Link>
    );

  const getDropDownItem = ({
    title,
    children = [],
  }: {
    title?: string;
    children?: MenuItem[];
  }) => {
    return (
      title &&
      children.length && (
        <DropDown
          button={<button>{title}</button>}
          animation={dropDownAnimationList.None}
          hover
        >
          <ul>
            {children.map((item, index) => (
              <li key={item?.page?.id || index}>{getLinkItem(item.page)}</li>
            ))}
          </ul>
        </DropDown>
      )
    );
  };

  return (
    <nav role="menu" className={classNames(styles.Nav, className)}>
      <ul>
        {menu.map(({ page, children = [] }, index) => {
          const key = page?.id || index;

          if (!page) {
            return null;
          }

          if (!children.length) {
            <li key={key}>{getLinkItem(page)}</li>;
          }

          return (
            <li key={key}>
              {getDropDownItem({ title: page.title, children })}
            </li>
          );
        })}
      </ul>
    </nav>
  );
};
