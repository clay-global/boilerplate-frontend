import React from "react";

import { PageData, AdditionalPageData } from "@/api/interface";

export interface Page {
  (props: {
    page: PageData;
    data: AdditionalPageData;
  }): React.ReactElement
}