import { slideScrollController } from "../useSlideScroll";

export class KeyboardControls {
  private id: string;
  private container: React.RefObject<HTMLDivElement | null>;

  constructor (id: string, container: React.RefObject<HTMLDivElement | null>) {
    this.id = id;
    this.container = container;
    document.addEventListener("keydown", this.handleKeyDown);
  }

  destructor = () => {
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  handleKeyDown = (e: KeyboardEvent) => {
    if (e.key === "Tab") {
      if (!this.container.current) return;
  
      const activeElement = document.activeElement;
      const activeSlideIndex = Array.from(this.container.current.children).reduce((prev, cur, index) => {
        return cur.contains(activeElement) ? index : prev;
      }, -1);

      if (activeSlideIndex > -1) {
        slideScrollController.slideTo(this.id, activeSlideIndex);
      }

    } else if (e.key === "ArrowDown" || e.key === "PageDown") {
      slideScrollController.slideDown(this.id);
    } else if (e.key === "ArrowUp" || e.key === "PageUp") {
      slideScrollController.slideUp(this.id);
    }
  }
}