import { FC } from "react";
import classNames from "classnames";

import { Link } from "@/feature/Link";
import { Image } from "@/feature/Image";

import { SvgIconLogo } from "./SvgIconLogo";
import styles from "./Logo.module.scss";

/** Remove or put your own variant here if you need */
export type LogoVariant = "white" | "dark";

export interface LogoProps {
  /** Link appearence variant */
  variant?: LogoVariant;
  /** Link title */
  title?: string;
  /** Image source if you use non svg icon */
  imageSrc?: string;
  className?: string;
}

export const Logo: FC<LogoProps> = ({
  variant = "dark",
  className,
  title,
  imageSrc,
}) => {
  const label = title || "Home";

  const image = imageSrc ? (
    <Image src={imageSrc} alt="label" />
  ) : (
    <SvgIconLogo variant={variant} className={styles.Icon} />
  );

  return (
    <Link
      href={"/"}
      target="_self"
      className={classNames(styles.Link, className)}
    >
      {image}
      {label}
    </Link>
  );
};
