import React from "react";
import { SlideScrollAnimation, SlideScrollAnimationOptions } from "./types";
import { transition } from "@/shared/utils/animation.utils";
import { cssEasings } from "@/app/config/ease.config";

export class ScrollAnimation implements SlideScrollAnimation {
  private slideRefs: Array<React.RefObject<HTMLElement | null>> = [];
  private container: React.RefObject<HTMLDivElement | null>;
  private topZIndex = 0;

  constructor(options: SlideScrollAnimationOptions) {
    this.slideRefs = options.slideRefs;
    this.container = options.container;
  }

  init = (currentIndex: number) => {
    this.slideRefs.forEach((ref, index) => {
      if (!ref.current) return;

      ref.current.style.zIndex = `${this.topZIndex++}`;

      if (index > currentIndex) {
        transition(ref.current, 0, {
          transform: "translateY(100%) translateZ(0px)"
        });
      } else if (index < currentIndex) {
        transition(ref.current, 0, {
          transform: "translateY(-100%) translateZ(0px)"
        });
      } else {

      }
    });
  }

  animate = (previousIndex: number, currentIndex: number, duration: number) => {
    const previousElement = this.slideRefs[previousIndex].current;
    const currentElement = this.slideRefs[currentIndex].current;

    if (currentIndex < previousIndex) {
    if (previousElement) {
      transition(previousElement, duration, {
        transform: "translateY(100%) translateZ(0px)"
      }, {
        ease: cssEasings.ease
      });
    }
  } else {
    if (previousElement) {
      transition(previousElement, duration, {
        transform: "translateY(-100%) translateZ(0px)"
      }, {
        ease: cssEasings.ease
      });
    }
  }

  if (currentElement) {
    currentElement.style.zIndex = `${this.topZIndex++}`;

    transition(currentElement, duration, {
      transform: "translateY(0) translateZ(0px)"
    }, {
      ease: cssEasings.ease
    });
  }
  }

  touchShift = (currentIndex: number, shift: number) => {
    if (!this.container.current) return;
    transition(this.container.current, 0, {
      transform: `translateY(${shift}px) translateZ(0px)`
    });
  }

  touchShiftCancel = (duration: number) => {
    if (!this.container.current) return;
    transition(this.container.current, 0.6 * duration, {
      transform: `translateY(0px) translateZ(0px)`
    }, {
      ease: cssEasings.ease
    });
  }
}