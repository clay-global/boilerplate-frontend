import classNames from "classnames";

import { PageData } from "@/api/interface";
import { MenuItem } from "@/api/requests/getMenu";
import { Logo } from "@/feature/Logo";
import { Nav } from "@/feature/Nav";
import { Link } from "@/feature/Link";

import styles from "./Header.module.scss";

interface HeaderData extends PageData {}

interface HeaderAdditionalData {
  menu: MenuItem[];
}

export interface Header {
  (props: {
    page: HeaderData;
    data: HeaderAdditionalData;
    className?: string;
  }): React.ReactElement;
}

export const Header: Header = ({ page, data, className }) => {
  const { menu } = data;

  return (
    <header className={classNames(styles.Header, className)}>
      <Logo />
      {menu && <Nav menu={menu} />}
      <Link href="#">Call to action</Link>
    </header>
  );
};
