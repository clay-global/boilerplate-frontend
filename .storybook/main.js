module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "storybook-readme",
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-storysource",
    "@storybook/addon-a11y",
    "storybook-css-modules-preset",
  ],
  "framework": "@storybook/react",
  "staticDirs": ['../public', '../src/stories/assets/'],
  "webpackFinal": async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    config.module.rules.push({
      test: /\.md$/i,
      use: [
        {
          loader: 'raw-loader',
          options: {
            esModule: true,
          },
        },
      ],
    },);

    // Return the altered config
    return config;
  },
}
