import type { Page } from "./interface";
import { Home } from "./Home";

/**
 * Page Component relevant to the page type (from the api)
 */
export const pagesByType: {
  [index: string] : Page
} = {
  home: Home
}

export type {
  Page
}

export { Home }
