import { requestError } from '../error';
import { PageShortData, ImageData } from '../interface';

const route = `${process.env.API_URL}navs/menu_top/tree`;

export const isMenuFullPageData = (
  item: MenuFullPageData | MenuLimitedPageData,
): item is MenuFullPageData => {
  return typeof item.uri === 'string';
};

interface MenuFullPageData extends PageShortData {
  icon?: ImageData;
  subtitle?: string;
}

interface MenuLimitedPageData {
  id: string;
  title: string;
  uri: null;
}

export interface MenuItem {
  page: MenuFullPageData | MenuLimitedPageData;
  depth: number;
  children: MenuItem[];
}

export type MenuData = MenuItem[];

interface MenuResponse {
  data: MenuData;
}

export const getMenu = async () => {
  try {
    const res = await fetch(route);
    const json: MenuResponse = await res.json();

    return json.data;
  } catch (err) {
    console.error(requestError(route, err));

    return [];
  }
};
