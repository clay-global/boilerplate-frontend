import { ScrollAnimation } from "./ScrollAnimation";
import { NoAnimation } from "./NoAnimation";
import { SlideScrollAnimation, SlideScrollAnimationOptions } from "./types";

export const slideScrollAnimationList = {
  none: NoAnimation,
  scroll: ScrollAnimation
}

export type {
  SlideScrollAnimation,
  SlideScrollAnimationOptions
}