import { requestError } from "../error";
import { PageData } from "../interface";

let params = new URLSearchParams({
  fields: "id,type,title,slug,uri,url,parent,blueprint,last_modified",
});
params.append("filter[published]", "true");

const route = `${process.env.API_URL}collections/pages/entries?${params}`;

interface GetPages {
  (): Promise<PageData[]>
}

export const getPages:GetPages = async() => {
  try {
    const res = await fetch(route);
    const json = await res.json();
  
    const result: PageData[] = json.data.map((p:any) => {
      const entry: PageData = p;
  
      return entry;
    });
  
    return result;
  } catch (err) {
    throw new Error(requestError(route, err));
  }
}