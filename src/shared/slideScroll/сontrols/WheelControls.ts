import { slideScrollController } from "../useSlideScroll";

export class WheelControls {
  private id: string;
  private scrollBuffer: number[] = [];
  private bufferLength = 40;
  private isCtrlPressed = false;
  private eventTimeStamp: number | undefined;

  constructor (id: string) {
    this.id = id;

    this.resetBuffer();

		document.addEventListener("wheel", this.handleWheel);
    document.addEventListener("keydown", this.handleKeyDown);
    document.addEventListener("keyup", this.handleKeyUp);
  }

  destructor = () => {
		document.removeEventListener("wheel", this.handleWheel);
    document.removeEventListener("keydown", this.handleKeyDown);
    document.removeEventListener("keyup", this.handleKeyUp);
  }

  resetBuffer = () => {
		this.scrollBuffer = [];
		for (let i = 0; i < this.bufferLength; i++) {
			this.scrollBuffer.push(0);
		}
	}

  handleKeyDown = (e: KeyboardEvent) => {
    if (e.key === "Control") {
      this.isCtrlPressed = true;
    }
  }

  handleKeyUp = () => {
    this.isCtrlPressed = false;
  }

  handleWheel = (e: WheelEvent) => {
		const value = -e.deltaY || -e.detail;
		const direction = value > 0 ? "up" : value < 0 ? "down" : "none";

		if (this.isCtrlPressed) return;
		if ( Math.abs(e.deltaX) > Math.abs(e.deltaY) )
			return;

		const previousTime = this.eventTimeStamp;
		this.eventTimeStamp = new Date().getTime();

		if (previousTime && this.eventTimeStamp - previousTime > 300) {
			this.resetBuffer();
		}

		this.scrollBuffer.push(Math.abs(value));
		this.scrollBuffer.shift();

		const newValues = this.scrollBuffer.slice(30, 40);
		const oldValues = this.scrollBuffer.slice(0, 30);

		const meanNewValue = newValues.reduce(function (previousValue, currentValue) {
			return previousValue + currentValue;
		}) / 10;

		const meanOldValue = oldValues.reduce(function (previousValue, currentValue) {
			return previousValue + currentValue;
		}) / 30;

		if (meanNewValue > meanOldValue) {
      if (direction === "up") {
        slideScrollController.slideUp(this.id);
      } else if (direction === "down") {
        slideScrollController.slideDown(this.id);
      }
		}
	}
}