import { GetStaticProps } from "next";
import { ReactElement } from "react";
import { additionalDataByType, additionalDataDefault } from "@/api/additioanalPageData";
import { PageData, AdditionalPageData } from "@/api/interface";
import { Page } from "@/pages/interface";

export const getStaticProps:GetStaticProps = async() => {
  const type = "404";
  const additionalDataRequests = {...additionalDataDefault, ...additionalDataByType[type]};

  const data: AdditionalPageData = {};

  for (const key of Object.keys(additionalDataRequests)) {
    data[key] = await additionalDataRequests[key]();
  }

  return {
    props: {
      page: {
        title: "Not Found",
        description: "",
        blueprint: {
          handle: "404"
        }
      },
      data,
    }
  }
}

interface NotFound extends Page {
  (props: {
    page: null,
    data: null
  }): ReactElement
}

export const NotFound: NotFound = () => {
  return (
    <></>
  )
}

export default NotFound;