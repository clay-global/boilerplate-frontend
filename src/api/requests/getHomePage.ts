import { requestError } from '../error';
import { PageData } from '../interface';

const route = `${process.env.API_URL}collections/pages/entries/home`;
const responseMock = {
  blueprint: {
    handle: '',
    title: '',
  },
  date: '',
  id: '',
  last_modified: '',
  locale: '',
  parent: {
    id: '',
    title: '',
    api_url: '',
  },
  private: false,
  published: false,
  slug: '',
  status: '',
  title: '',
  updated_at: '',
  uri: '',
  url: '',
};

interface HomePageResponse {
  data: PageData;
}

export const getHomePage = async () => {
  try {
    const res = await fetch(route);
    const json: HomePageResponse = await res.json();

    return json.data;
  } catch (err) {
    console.error(requestError(route, err));

    return responseMock;
  }
};
