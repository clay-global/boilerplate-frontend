import { GetStaticProps } from "next";
import { Home } from "@/pages/Home";
import { getHomePage } from "@/api/requests/getHomePage";
import {
  additionalDataByType,
  additionalDataDefault,
} from "@/api/additioanalPageData";
import { PageData, AdditionalPageData } from "@/api/interface";

export const getStaticProps: GetStaticProps = async () => {
  const page: PageData = await getHomePage();

  const type: string = page.blueprint.handle;
  const additionalDataRequests = {
    ...additionalDataDefault,
    ...additionalDataByType[type],
  };

  const data: AdditionalPageData = {};

  for (const key of Object.keys(additionalDataRequests)) {
    data[key] = await additionalDataRequests[key]();
  }

  return {
    props: JSON.parse(
      JSON.stringify({
        page,
        data,
      })
    ),
  };
};

interface Index {
  (props: { page: PageData; data: AdditionalPageData }): React.ReactElement;
}

const Index: Index = ({ page, data }) => {
  return <Home page={page} data={data}></Home>;
};

export default Index;
