import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import { slideScrollController, privateEventEmitter } from "./useSlideScroll";
import { KeyboardControls } from "./сontrols/KeyboardControls";
import { WheelControls } from "./сontrols/WheelControls";
import { TouchControls } from "./сontrols/TouchControls";
import {
  slideScrollAnimationList,
  SlideScrollAnimation,
  SlideScrollAnimationOptions,
} from "./animations";
import { useScrollLock } from "../scroll";

export interface SlideScroll {
  (props: {
    id: string;
    className?: string;
    classNameActiveElement?: string;
    classNamePreviousElement?: string;
    classNameNextElement?: string;
    lockScroll?: boolean;
    disabled?: boolean;
    animation?: new (
      ...args: [animationOptions: SlideScrollAnimationOptions]
    ) => SlideScrollAnimation;
    animationDuration?: number;
    lockDuration?: number;
    children: React.ReactElement[];
  }): React.ReactElement;
}

export const SlideScroll: SlideScroll = ({
  id,
  className,
  classNameActiveElement = "slide_active",
  classNamePreviousElement = "slide_previous",
  classNameNextElement = "slide_next",
  lockScroll = true,
  disabled = false,
  animation = slideScrollAnimationList.none,
  animationDuration = 0.6,
  lockDuration = 0.45,
  children,
}) => {
  const lockScrollController = useScrollLock().lockScroll;
  const unlockScrollController = useScrollLock().unlockScroll;
  const previousIndexRef = useRef(0);

  const modules = useRef<{
    animation: SlideScrollAnimation | undefined;
    keyboardControls: KeyboardControls | undefined;
    wheelControls: WheelControls | undefined;
    touchControls: TouchControls | undefined;
  }>({
    animation: undefined,
    keyboardControls: undefined,
    wheelControls: undefined,
    touchControls: undefined,
  });

  const containerRef = useRef<HTMLDivElement | null>(null);

  const childrenRefs: React.MutableRefObject<React.RefObject<HTMLElement>[]> =
    useRef([]);

  const handleEventEmitter = useCallback(() => {
    const state = slideScrollController.getStateById(id);
    if (!state) return;

    childrenRefs.current.forEach((ch, index) => {
      if (!ch.current) return;

      if (index === state.index) {
        ch.current.classList.add(classNameActiveElement);
        ch.current.classList.remove(classNameNextElement);
        ch.current.classList.remove(classNamePreviousElement);
      } else if (index > state.index) {
        ch.current.classList.remove(classNameActiveElement);
        ch.current.classList.add(classNameNextElement);
        ch.current.classList.remove(classNamePreviousElement);
      } else if (index < state.index) {
        ch.current.classList.remove(classNameActiveElement);
        ch.current.classList.remove(classNameNextElement);
        ch.current.classList.add(classNamePreviousElement);
      }
    });

    modules.current.animation?.animate(
      previousIndexRef.current,
      state.index,
      animationDuration
    );

    previousIndexRef.current = state.index;

    return () => {
      childrenRefs.current.forEach((ch) => {
        if (!ch.current) return;

        ch.current.classList.remove(classNameActiveElement);
        ch.current.classList.remove(classNameNextElement);
        ch.current.classList.remove(classNamePreviousElement);
      });
    };
  }, [
    animation,
    animationDuration,
    classNameActiveElement,
    classNameNextElement,
    classNamePreviousElement,
  ]);

  // const handleResize = useCallback(() => {
  // }, [elements]);

  // useEffect(() => {
  //   syncResize.subscribe(handleResize);

  //   return () => {
  //     syncResize.unsubscribe(handleResize);
  //   }
  // }, [handleResize]);

  // Initialize state
  useEffect(() => {
    const state = slideScrollController.getStateById(id);
    if (state) {
      previousIndexRef.current = state.index;
      privateEventEmitter.dispatch({
        type: "slideScroll:set",
        payload: {
          id,
          item: {
            index: state.index,
            total: children.length,
            animationDuration,
            lockDuration,
          },
        },
      });
    } else {
      previousIndexRef.current = 0;
      privateEventEmitter.dispatch({
        type: "slideScroll:add",
        payload: {
          id,
          item: {
            index: 0,
            total: children.length,
            animationDuration,
            lockDuration,
          },
        },
      });
    }

    return () => {
      privateEventEmitter.dispatch({
        type: "slideScroll:remove",
        payload: {
          id,
        },
      });
    };
  }, []);

  // Prepare elements
  useEffect(() => {
    if (!containerRef.current) return;
    const state = slideScrollController.getStateById(id);

    childrenRefs.current.length = 0;

    Array.from(containerRef.current.children).forEach((el, index) => {
      const ref: React.MutableRefObject<HTMLElement | null> = React.createRef();
      childrenRefs.current.push(ref);

      const element = el as HTMLElement;
      ref.current = element;
      element.style.position = "absolute";
      element.style.height = "100%";
      element.style.width = "100%";
      element.style.left = "0";
      element.style.top = "0";
      element.style.zIndex = `${index}`;

      if (state) {    
        if (index === state.index) {
          element.classList.add(classNameActiveElement);
          element.classList.remove(classNameNextElement);
          element.classList.remove(classNamePreviousElement);
        } else if (index > state.index) {
          element.classList.remove(classNameActiveElement);
          element.classList.add(classNameNextElement);
          element.classList.remove(classNamePreviousElement);
        } else if (index < state.index) {
          element.classList.remove(classNameActiveElement);
          element.classList.remove(classNameNextElement);
          element.classList.add(classNamePreviousElement);
        }
      }
    });
  }, [children, classNameNextElement, classNameNextElement, classNameActiveElement]);

  // Main event handler
  useEffect(() => {
    slideScrollController.subscribe(handleEventEmitter);

    return () => {
      slideScrollController.unsubscribe(handleEventEmitter);
    };
  }, [handleEventEmitter]);

  // Scroll Lock
  useEffect(() => {
    window.scrollTo(0, 0);

    if (lockScroll && !disabled) {
      setTimeout(() => {
        lockScrollController(`SLIDE_SCROLL_${id}`);
      }, 0);
    }

    return () => {
      setTimeout(() => {
        unlockScrollController(`SLIDE_SCROLL_${id}`);
      }, 0);
    };
  }, [id, lockScroll, disabled]);

  // Modules
  useEffect(() => {
    if (!modules.current.animation) {
      modules.current.animation = new animation({
        slideRefs: childrenRefs.current,
        container: containerRef,
      });
    }

    if (!disabled) {
      modules.current.keyboardControls = new KeyboardControls(id, containerRef);
      modules.current.wheelControls = new WheelControls(id);
      modules.current.touchControls = new TouchControls(
        id,
        containerRef,
        modules.current.animation
      );
    }

    return () => {
      if (modules.current.keyboardControls) {
        modules.current.keyboardControls.destructor();
        modules.current.keyboardControls = undefined;
      }
      if (modules.current.wheelControls) {
        modules.current.wheelControls.destructor();
        modules.current.wheelControls = undefined;
      }
      if (modules.current.touchControls) {
        modules.current.touchControls.destructor();
        modules.current.touchControls = undefined;
      }
    };
  }, [id, disabled]);

  // Init animation styles
  useEffect(() => {
    const state = slideScrollController.getStateById(id);
    if (!state) return;

    modules.current.animation?.init(state.index);
  }, [children, animation, id]);

  return (
    <div ref={containerRef} className={className}>
      {children}
    </div>
  );
};
