import { useCallback } from "react";
import { EventEmitter } from "../utils/EventEmitter";

type CarouselEventTypes =
  | "carousel:to"
  | "carousel:toIndex"
  | "carousel:toPercent"
  | "carousel:toVw"
  | "carousel:by"
  | "carousel:byIndex"
  | "carousel:byPercent"
  | "carousel:byVw";

interface CarouselState {
  /**
   * Carousle Id
   */
  id: string;
  /**
   * Track position in pixels
   */
  carouselPosition: number;
  /**
   * Container width
   */
  containerWidth: number;
  /**
   * Track width
   */
  totalWidth: number;
  /**
   * Number of elements
   */
  numberOfElements: number; 
  /**
   * Carousel element width
   */
  itemWidth: number;
  /**
   * Width of the gap between elements
   */
  gapWidth: number;
  /**
   * Carousel movement speed
   */
  speed: number;
  /**
   * Carousel alignment
   */
  align: "left" | "center";
}

export interface CarouselBasicEvent {
  type: CarouselEventTypes;
  payload: {
    id: string;
    scrollValue: number;
  };
}

export interface CarouselChangePositionEvent {
  type: "carousel:change-position";
  payload: CarouselState;
}

interface CarouselPublicEvent {
  type: "carousel:change-position",
  payload: {}
}

const carouselGlobalState: {
  [key: string]: CarouselState | undefined;
} = {}

export const carouselPrivateEventEmitter = new EventEmitter<CarouselBasicEvent | CarouselChangePositionEvent>();
const carouselPublicEventEmitter = new EventEmitter<CarouselPublicEvent>();

export const isBasicEvent = (event: CarouselBasicEvent | CarouselChangePositionEvent): event is CarouselBasicEvent => {
  return event.type !== "carousel:change-position";
}

carouselPrivateEventEmitter.subscribe((event) => {
  if (isBasicEvent(event)) return;

  carouselGlobalState[event.payload.id] = {
    id: event.payload.id,
    carouselPosition: event.payload.carouselPosition,
    containerWidth: event.payload.containerWidth,
    numberOfElements: event.payload.numberOfElements,
    itemWidth: event.payload.itemWidth,
    gapWidth: event.payload.gapWidth,
    speed: event.payload.speed,
    align: event.payload.align,
    totalWidth: event.payload.totalWidth
  }

  carouselPublicEventEmitter.dispatch({
    type: "carousel:change-position",
    payload: {}
  });
})

export const carouselController = {
  toPosition: (id: string, position: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:to",
      payload: {
        id,
        scrollValue: position,
      },
    });
  },
  toIndex: (id: string, index: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:toIndex",
      payload: {
        id,
        scrollValue: index,
      },
    });
  },
  toPercent: (id: string, percent: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:toPercent",
      payload: {
        id,
        scrollValue: percent,
      },
    });
  },
  toVw: (id: string, vw: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:toVw",
      payload: {
        id,
        scrollValue: vw,
      },
    });
  },
  byPosition: (id: string, position: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:by",
      payload: {
        id,
        scrollValue: position,
      },
    });
  },
  byIndex: (id: string, index: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:byIndex",
      payload: {
        id,
        scrollValue: index,
      },
    });
  },
  byPercent: (id: string, percent: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:byPercent",
      payload: {
        id,
        scrollValue: percent,
      },
    });
  },
  byVw: (id: string, vw: number) => {
    carouselPrivateEventEmitter.dispatch({
      type: "carousel:byVw",
      payload: {
        id,
        scrollValue: vw,
      },
    });
  },
  subscribe: (handler: (event: CarouselPublicEvent) => void) => {
    carouselPublicEventEmitter.subscribe(handler);
  },
  unsubscribe: (handler: (event: CarouselPublicEvent) => void) => {
    carouselPublicEventEmitter.unsubscribe(handler);
  },
  get: () => {
    return carouselGlobalState;
  },
  getById: (id: string) => {
    return carouselGlobalState[id];
  }
};

export const useCarousel = () => {
  const carouselToPosition = useCallback(carouselController.toPosition, []);
  const carouselToIndex = useCallback(carouselController.byIndex, []);
  const carouselToPercent = useCallback(carouselController.byPercent, []);
  const carouselToVw = useCallback(carouselController.toVw, []);
  const carouselByPosition = useCallback(carouselController.byPosition, []);
  const carouselByIndex = useCallback(carouselController.byIndex, []);
  const carouselByPercent = useCallback(carouselController.byPercent, []);
  const carouselByVw = useCallback(carouselController.byVw, []);

  return {
    carouselToPosition,
    carouselToIndex,
    carouselToPercent,
    carouselToVw,
    carouselByPosition,
    carouselByIndex,
    carouselByPercent,
    carouselByVw,
  };
};
