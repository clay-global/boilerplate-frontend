import { EventEmitter, Event } from "@/shared/utils/EventEmitter";
import { scrollLockController } from "@/shared/scroll/useScrollLock/useScrollLock";
import { useCallback, useEffect, useMemo, useState } from "react";

const IS_SCROLL_LOCKED = true;
const SCROLL_LOCK_KEY = "modal";

interface ModalItem {
  id: string;
}

interface ModalHandler {
  (e: Event): void;
}

const eventEmitter = new EventEmitter();

export const modalState: {
  modals: ModalItem[];
  activeModal: string | null;
} = {
  activeModal: null,
  modals: [],
};

export const modalController = {
  open: (id: string) => {
    if (modalState.activeModal === id) {
      return;
    }

    modalState.activeModal = id;
    eventEmitter.dispatch({
      type: "change",
    });

    IS_SCROLL_LOCKED && scrollLockController.lock(SCROLL_LOCK_KEY);
  },

  toggle: (id: string) => {
    if (modalState.activeModal === id) {
      modalState.activeModal = null;

      IS_SCROLL_LOCKED && scrollLockController.unlock(SCROLL_LOCK_KEY);
    } else {
      modalState.activeModal = id;

      IS_SCROLL_LOCKED && scrollLockController.lock(SCROLL_LOCK_KEY);
    }

    eventEmitter.dispatch({
      type: "change",
    });
  },

  close: (id: string) => {
    if (modalState.activeModal !== id) {
      return;
    }

    modalState.activeModal = null;

    IS_SCROLL_LOCKED && scrollLockController.unlock(SCROLL_LOCK_KEY);

    eventEmitter.dispatch({
      type: "change",
    });
  },

  closeAll: () => {
    if (modalState.activeModal) {
      return;
    }

    modalState.activeModal = null;

    IS_SCROLL_LOCKED && scrollLockController.unlock(SCROLL_LOCK_KEY);

    eventEmitter.dispatch({
      type: "change",
    });
  },

  subscribe: (handler: ModalHandler) => {
    eventEmitter.subscribe(handler);
  },

  unsubscribe: (handler: ModalHandler) => {
    eventEmitter.unsubscribe(handler);
  },

  getState: () => {
    return modalState;
  },
};

export const _modalControllerPrivate = {
  addModal: (modal: ModalItem) => {
    if (modalState.modals.find(m => m.id === modal.id)) {
      throw new Error(
        `Modal with id ${modal.id} already exists in the global state`
      );
    }

    modalState.modals.push(modal);
  },

  removeModal: (id: string) => {
    if (!modalState.modals.find((m) => m.id === id)) {
      throw new Error(`Modal with id ${id} doesn't exist in the global state`);
    }

    modalState.modals = modalState.modals.filter(m => m.id !== id);
  },
};

export const useModal = () => {
  const [activeModal, setActiveModal] = useState<string | null>(null);

  const handleModal = useCallback(() => {
    setActiveModal(modalController.getState().activeModal);
  }, []);

  const open = useCallback((id: string) => {
    modalController.open(id);
  }, []);

  const close = useCallback((id: string) => {
    modalController.close(id);
  }, []);

  const toggle = useCallback((id: string) => {
    modalController.toggle(id);
  }, []);

  const closeAll = useCallback((id: string) => {
    modalController.closeAll();
  }, []);

  useEffect(() => {
    modalController.subscribe(handleModal);

    return () => {
      modalController.unsubscribe(handleModal);
    };
  }, [handleModal]);

  return useMemo(() => {
    return {
      activeModal,
      open,
      close,
      toggle,
      closeAll,
    };
  }, [activeModal, open, close, toggle, closeAll]);
};
