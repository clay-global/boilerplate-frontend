import type { AppProps } from "next/app";
import Head from "next/head";
import "focus-visible";

import { keysToCamel } from "@/shared/utils/object.utils";
import "@/app/styles/index.css";

// Components
import { Layout } from "@/entity/Layout";

function App({ Component, pageProps: pageInitialProps }: AppProps) {
  // Remove if needed
  const pageProps = keysToCamel(pageInitialProps);

  const { data = {}, page = {} } = pageProps;

  return (
    <>
      <Head>
        <title>Title</title>
        <meta name="description" content={"Description"} />
        <link rel="shortcut icon" href="/images/favicon.ico" />
      </Head>
      <Layout page={page} data={data}>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}

export default App;
