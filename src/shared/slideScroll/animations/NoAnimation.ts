import React from "react";
import { SlideScrollAnimation, SlideScrollAnimationOptions } from "./types";
import { transition } from "@/shared/utils/animation.utils";
import { cssEasings } from "@/app/config/ease.config";

export class NoAnimation implements SlideScrollAnimation {
  private slideRefs: Array<React.RefObject<HTMLElement | null>> = [];
  private container: React.RefObject<HTMLDivElement | null>;
  private topZIndex = 0;

  constructor(options: SlideScrollAnimationOptions) {
    this.slideRefs = options.slideRefs;
    this.container = options.container;
  }

  init = () => {
    this.slideRefs.forEach((ref, index) => {
      if (!ref.current) return;

      ref.current.style.zIndex = `${this.topZIndex++}`;
    });
  };

  animate = (previousIndex: number, currentIndex: number) => {
    const previousElement = this.slideRefs[previousIndex].current;
    const currentElement = this.slideRefs[currentIndex].current;

    if (currentElement) {
      currentElement.style.zIndex = `${this.topZIndex++}`;
    }
  };

  touchShift = (currentIndex: number, shift: number) => {
    if (!this.container.current) return;
    transition(this.container.current, 0, {
      transform: `translateY(${shift}px) translateZ(0px)`,
    });
  };

  touchShiftCancel = (duration: number) => {
    if (!this.container.current) return;
    transition(
      this.container.current,
      0.6 * duration,
      {
        transform: `translateY(0px) translateZ(0px)`,
      },
      {
        ease: cssEasings.ease,
      }
    );
  };
}
