import { addReadme } from 'storybook-readme';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

const customViewports = {
  Phone: {
    name: '375 x 415 (Phone)',
    styles: {
      width: '375px',
      height: '415px',
    },
  },
  Tablet: {
    name: '768 x 1280 (Tablet)',
    styles: {
      width: '768px',
      height: '1280px',
    },
  },
  Desktop: {
    name: '1440 x 1280 (Desktop)',
    styles: {
      width: '1440px',
      height: '1280px',
    },
  },
};

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  viewport: {
    viewports: {
      ...customViewports,
      ...INITIAL_VIEWPORTS,
    }
  },
}

export const decorators = [addReadme]