import { requestError } from '../error';
import { ImageData } from '../interface';

const route = `${process.env.API_URL}Settings/settings`;
const responseMock = {
  meta_title_prefix: null,
  meta_description_default: null,
  google_analytics_id: null,
  meta_social_image: null,
};

export interface SettingsData {
  meta_title_prefix: string | null;
  meta_description_default: string | null;
  google_analytics_id: string | null;
  meta_social_image: ImageData | null;
}

interface ContactsResponse {
  data: SettingsData;
}

export const getSettings = async () => {
  try {
    const res = await fetch(route);
    const json: ContactsResponse = await res.json();

    return json.data;
  } catch (err) {
    console.error(requestError(route, err));

    return responseMock;
  }
};
