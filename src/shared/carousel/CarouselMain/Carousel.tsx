import React, {
  useCallback,
  useEffect,
  useState,
  useRef,
  useLayoutEffect,
  CSSProperties,
} from "react";
import { syncResize } from "../../resize";
import { CarouselItem } from "./CarouselItem";
import { Resize, ResizeSimple, ResizeContinuous } from "./resize";
import { Motion, MotionCallback } from "./motion";
import { PointerControls, PointerCallback } from "./pointerControls";
import { simpleEase } from "../../utils/ease.utils";
import {
  CarouselBasicEvent,
  CarouselChangePositionEvent,
  carouselPrivateEventEmitter,
  isBasicEvent,
} from "../useCarousel";
import { FocusControls } from "./focusControls";
import { TabControls } from "./tabControls";
import { ChildrenAnimation, noChildrenAnimation } from "./animations/children/";
import { CompositeAnimation } from "../../utils/animation.utils";
import { noTriggerAnimation, TriggerAnimation } from "./animations/trigger";
import styles from "./Carousel.module.css";

// WIP: Focus controls to check what current index is and move to next. On continuous scroll to closest, rather then index

interface CarouselSizes {
  containerWidth: number;
  itemWidth: number;
  gapWidth: number;
  totalWidth: number;
  leftShift: number;
  numberOfFakeElements: number;
}

export interface CarouselProps {
  id: string;
  /**
   * Carousel mode. Should it be continuous or should it be limited by the wrapper.
   */
  mode?: "simple" | "continuous";
  /**
   * Carousel alignment.
   */
  align?: "left" | "center";
  /**
   * Carousel drag. If "auto", then carousel will be draggable only if its track width is higher than the wrapper width. Will be ignored if mode is set to continuous.
   */
  interactive?: "always" | "auto" | "never";
  /**
   * Idle scrolling animation speed. Pixels per frame. Default is 0.
   */
  idleScroll?: number;
  /**
   * Smooth will use inertion and decay to add acceleration & decceleration. Usefull when idleScroll is being added after the carousel was created, e.g. on mouse enter.
   */
  idleScrollMode?: "smooth" | "simple";
  /**
   * Disable scrolling animation on the first interaction.
   */
  idleScrollDisableOnInteraction?: boolean;
  /**
   * Carousel will resize it's elements according to the elementMaxWidth and elementsPerScreen properties.
   */
  resizeElements?: boolean;
  /**
   * Gap between the elements in pixels
   */
  gap?: number;
  /**
   * Additional width outside of the container
   */
  padding?: number;
  /**
   * Outer padding measurment units
   */
  paddingUnits?: "px" | "vw" | "%";
  /**
   * Width of the elements in pixels
   */
  itemWidth?: number;
  /**
   * Maximum width of the children in pixels
   */
  maximumItemWidth?: number;
  /**
   * Number of displayed children per breakpoint. The syntax looks like this: [{breakpoint: 320, number: 1}, {breakpoint: 640, number: 2}, {breakpoint: 1000, number:3}]
   */
  elementsPerScreen?: {
    breakpoint: number;
    number: number;
  }[];
  /**
   * Maximum element width.
   */
  elementMaxWidth?: number;
  /**
   * Carousel will try to auto-align its children with the wrapper.
   */
  inertionCorrection?: boolean;
  /**
   * Inertion decay factor
   */
  inertionDecay?: number;
  /**
   * Inertion interpolation function. Function that is used to calculate the inertion.
   */
  inertionInterpolationFunction?: (
    value: number,
    targetValue: number,
    inertionDecay: number
  ) => number;
  /**
   * Applies a fix to the parent section. When user tabs through the interactive elements inside of the carousel, browser may decide to scroll the closest "overflow:hidden" element, which is ususally a section.
   */
  applySectionScrollFix?: boolean;
  /**
   * Prop for trigger animation
   */
  triggered?: boolean;
  /**
   * Function that handles the trigger animation
   */
  triggerFunction?: TriggerAnimation;
  /**
   * Animation function for children
   */
  childrenAnimationFunction?: ChildrenAnimation;
  /**
   * Aria Label that will be added to let users know that carousel is controllable with keyboard
   */
  ariaLabel?: string;
  className?: string;
  style?: CSSProperties;
  children: React.ReactElement[];
}

const useIsomorphicLayoutEffect =
  typeof window !== "undefined" ? useLayoutEffect : useEffect;

export const Carousel = ({
  id,
  mode = "simple",
  inertionCorrection = false,
  align = "left",
  interactive = "always",
  idleScroll = 0,
  idleScrollMode = "simple",
  idleScrollDisableOnInteraction = true,
  gap = 30,
  padding = 0,
  paddingUnits = "px",
  itemWidth = 300,
  maximumItemWidth = 350,
  resizeElements = true,
  elementsPerScreen = [
    {
      breakpoint: 320,
      number: 1,
    },
    {
      breakpoint: 640,
      number: 2,
    },
    {
      breakpoint: 1024,
      number: 3,
    },
  ],
  inertionDecay = 16,
  inertionInterpolationFunction = simpleEase,
  applySectionScrollFix = true,
  triggered = false,
  childrenAnimationFunction = noChildrenAnimation,
  triggerFunction = noTriggerAnimation,
  ariaLabel = "Carousel. Use arrow keys to change slides.",
  className,
  style,
  children,
}: CarouselProps) => {
  /**
   * Carousel track element
   */
  const trackElementRef = useRef<HTMLDivElement | null>(null);

  /**
   * Carousel html element
   */
  const wrapperElementRef = useRef<HTMLDivElement | null>(null);

  /**
   * Internal persistant values
   */
  const valuesRef = useRef<{
    /**
     * Motion values of the track
     */
    motion: {
      x: number;
      delta: number;
    };
    /**
     * Used in the resize functon to align the children
     */
    elementsPerScreen: {
      breakpoint: number;
      number: number;
    }[];
    /**
     * Actual sizes of the carusel and its children
     */
    carouselSizes: {
      containerWidth: number;
      itemWidth: number;
      gapWidth: number;
      totalWidth: number;
      leftShift: number;
      numberOfFakeElements: number;
    };
    /**
     * Position values
     */
    position: {
      actual: number;
      unshifted: number;
      unnormalized: number;
    };
    /**
     * If element was triggered already
     */
    isTriggered: boolean;
    /**
     * If trigger was initialized
     */
    isTriggerInitialized: boolean;
    /**
     * If carousel was interacted with
     */
    isInteractedWith: boolean;
  }>({
    motion: {
      x: 0,
      delta: 0,
    },
    elementsPerScreen: [],
    carouselSizes: {
      containerWidth: 0,
      itemWidth: 0,
      gapWidth: 0,
      totalWidth: 0,
      leftShift: 0,
      numberOfFakeElements: 0,
    },
    position: {
      actual: 0,
      unshifted: 0,
      unnormalized: 0,
    },
    isTriggered: triggered,
    isTriggerInitialized: false,
    isInteractedWith: false,
  });

  const modulesRef = useRef<{
    resize: Resize | null;
    motion: Motion | null;
    focusControls: FocusControls | null;
    pointerControls: PointerControls | null;
    tabControls: TabControls | null;
  }>({
    resize: null,
    motion: null,
    focusControls: null,
    pointerControls: null,
    tabControls: null,
  });

  /**
   * Carousel Children
   */
  const [items, setItems] = useState<JSX.Element[]>([]);

  /**
   * Calculating element per screen
   */
  if (
    valuesRef.current.elementsPerScreen.length !== elementsPerScreen.length ||
    !valuesRef.current.elementsPerScreen.reduce((prev, current, index) => {
      return (
        prev &&
        current.breakpoint === elementsPerScreen[index].breakpoint &&
        current.number === elementsPerScreen[index].number
      );
    }, true)
  ) {
    valuesRef.current.elementsPerScreen = elementsPerScreen;
  }

  /**
   * Total number of children (except for the fake ones, created by carousel)
   */
  const numberOfElements = children.length;

  /**
   * Refs to the children
   */
  let childrenRefs: React.MutableRefObject<
    React.RefObject<HTMLDivElement | null>[]
  > = useRef([]);

  /**
   * Indexes and refs to children, including fake ones. Used for animations
   */
  let animatedChildredRef = useRef<
    {
      index: number;
      fake: boolean;
      ref: React.RefObject<HTMLDivElement | null>;
      compositeAnimationRef: React.RefObject<CompositeAnimation | null>;
    }[]
  >([]);

  /**
   * State of the carousel sizes
   */
  const [carouselSizesState, setCarouselSizesState] = useState<CarouselSizes>(
    valuesRef.current.carouselSizes
  );

  /**
   * State with all the carousel wrapper styles
   */
  const [wrapperStylesState, setwrapperStylesState] = useState<CSSProperties>(
    style || {}
  );

  /**
   * Sets main track position
   */
  const setPosition = useCallback(
    (forced?: boolean) => {
      if (!trackElementRef.current) return;

      let position = valuesRef.current.motion.x;
      let delta = valuesRef.current.motion.delta;

      const unnormalizedPosition = position;
      valuesRef.current.position.unnormalized = position;

      if (
        mode === "continuous" &&
        valuesRef.current.carouselSizes.totalWidth > 0
      ) {
        while (position < -valuesRef.current.carouselSizes.totalWidth) {
          position += valuesRef.current.carouselSizes.totalWidth;
        }

        while (position > 0) {
          position -= valuesRef.current.carouselSizes.totalWidth;
        }
      }

      const unshiftedPosition = position;
      valuesRef.current.position.unshifted = position;

      position += valuesRef.current.carouselSizes.leftShift;

      if (align === "center") {
        position +=
          valuesRef.current.carouselSizes.containerWidth / 2 -
          valuesRef.current.carouselSizes.itemWidth / 2;
      }

      if (
        forced ||
        Math.abs(position - valuesRef.current.position.actual) > 0.05
      ) {
        carouselPrivateEventEmitter.dispatch({
          type: "carousel:change-position",
          payload: {
            id,
            carouselPosition: unshiftedPosition,
            containerWidth: valuesRef.current.carouselSizes.containerWidth,
            itemWidth: valuesRef.current.carouselSizes.itemWidth,
            gapWidth: valuesRef.current.carouselSizes.gapWidth,
            speed: delta,
            numberOfElements,
            align: align,
            totalWidth: valuesRef.current.carouselSizes.totalWidth,
          },
        });

        childrenAnimationFunction({
          carouselPosition: unshiftedPosition,
          unnormalizedCarouselPosition: unnormalizedPosition,
          containerWidth: valuesRef.current.carouselSizes.containerWidth,
          elements: animatedChildredRef.current.map((ch, i) => {
            return {
              ref: ch.ref,
              fake: ch.fake,
              index: ch.index,
              compositeAnimationRef: ch.compositeAnimationRef,
              position:
                (i - valuesRef.current.carouselSizes.numberOfFakeElements) *
                (valuesRef.current.carouselSizes.itemWidth +
                  valuesRef.current.carouselSizes.gapWidth),
            };
          }),
          itemWidth: valuesRef.current.carouselSizes.itemWidth,
          gapWidth: valuesRef.current.carouselSizes.gapWidth,
          speed: delta,
          align: align,
          totalWidth: valuesRef.current.carouselSizes.totalWidth,
        });

        valuesRef.current.position.actual = position;
        trackElementRef.current.style.transform = `translateX(${position}px) translateZ(0px)`;
      }
    },
    [mode, align, childrenAnimationFunction]
  );

  /**
   * Resize handler. Handles track size, children sizes and the number of fake elements, needed for a contionius carousel to work
   */
  const handleResize = useCallback(() => {
    if (!modulesRef.current.resize) return;

    const sizes = modulesRef.current.resize.resize();

    if (sizes) {
      valuesRef.current.carouselSizes = {
        containerWidth: sizes.containerWidth,
        gapWidth: sizes.gapWidth,
        itemWidth: sizes.itemWidth,
        totalWidth: sizes.totalWidth,
        leftShift: sizes.leftShift,
        numberOfFakeElements: sizes.numberOfFakeElements,
      };

      setCarouselSizesState(valuesRef.current.carouselSizes);
      setPosition();
    }
  }, [setPosition]);

  /**
   * Motion handler. Fires when positon of the track changes for whatever reason
   */
  const handleMotion = useCallback<MotionCallback>(
    (motionData) => {
      valuesRef.current.isInteractedWith = motionData.isInteractedWith;
      if (
        Math.abs(valuesRef.current.motion.x - motionData.position) > 0.05 ||
        Math.abs(valuesRef.current.motion.delta - motionData.delta)
      ) {
        valuesRef.current.motion.x = motionData.position;
        valuesRef.current.motion.delta = motionData.delta;

        setPosition();
      }
    },
    [setPosition]
  );

  /**
   * Drag & Touch handler
   */
  const handlePointer = useCallback<PointerCallback>(
    ({ dragging, pointerX, touchAction }) => {
      if (!modulesRef.current.pointerControls) return;

      if (dragging) {
        setwrapperStylesState((prevStyles) => {
          return prevStyles.cursor === "grabbing" &&
            prevStyles.touchAction === touchAction
            ? prevStyles
            : {
                ...prevStyles,
                cursor: "grabbing",
                touchAction: touchAction ? touchAction : prevStyles.touchAction,
              };
        });
      } else {
        setwrapperStylesState((prevStyles) => {
          return prevStyles.cursor === "grab" &&
            prevStyles.touchAction === touchAction
            ? prevStyles
            : {
                ...prevStyles,
                cursor: "grab",
                touchAction: touchAction ? touchAction : prevStyles.touchAction,
              };
        });
      }

      if (modulesRef.current.motion) {
        modulesRef.current.motion.applyDrag({
          isDragActive: dragging,
          dragPosition: pointerX,
        });
      }
    },
    []
  );

  /**
   * Carousel event handler
   */
  const handleEvent = useCallback(
    (event: CarouselBasicEvent | CarouselChangePositionEvent) => {
      if (!isBasicEvent(event)) return;
      if (!event || event.payload.id !== id || !modulesRef.current.motion) {
        return;
      }

      let position =
        event.type === "carousel:to"
          ? -event.payload.scrollValue
          : event.type === "carousel:toIndex"
          ? -event.payload.scrollValue *
            (valuesRef.current.carouselSizes.itemWidth +
              valuesRef.current.carouselSizes.gapWidth)
          : event.type === "carousel:toPercent"
          ? -event.payload.scrollValue *
            (valuesRef.current.carouselSizes.totalWidth -
              valuesRef.current.carouselSizes.containerWidth)
          : event.type === "carousel:toVw"
          ? -event.payload.scrollValue * syncResize.get().width
          : event.type === "carousel:by"
          ? valuesRef.current.position.unnormalized - event.payload.scrollValue
          : event.type === "carousel:byIndex"
          ? valuesRef.current.position.unnormalized -
            event.payload.scrollValue *
              (valuesRef.current.carouselSizes.itemWidth +
                valuesRef.current.carouselSizes.gapWidth)
          : event.type === "carousel:byPercent"
          ? valuesRef.current.position.unnormalized -
            (event.payload.scrollValue / 100) *
              valuesRef.current.carouselSizes.containerWidth
          : event.type === "carousel:byVw"
          ? valuesRef.current.position.unnormalized -
            event.payload.scrollValue * syncResize.get().width
          : 0;

      if (mode === "simple") {
        position = Math.min(
          0,
          Math.max(
            -(
              valuesRef.current.carouselSizes.totalWidth -
              valuesRef.current.carouselSizes.containerWidth
            ),
            position
          )
        );
      }

      if (Math.abs(position - valuesRef.current.position.unnormalized) < 0.1) {
        return;
      }

      modulesRef.current.motion.applyTransition({
        to: position,
      });
    },
    [id, mode]
  );

  /**
   * Init event emitter and resize handlers
   */
  useEffect(() => {
    carouselPrivateEventEmitter.subscribe(handleEvent);
    syncResize.subscribe(handleResize);

    return () => {
      carouselPrivateEventEmitter.unsubscribe(handleEvent);
      syncResize.unsubscribe(handleResize);
    };
  }, [handleEvent, handleResize]);

  /**
   * Intit resize module
   */
  useIsomorphicLayoutEffect(() => {
    if (trackElementRef.current && wrapperElementRef.current) {
      const options = {
        mode,
        resizeElements,
        elementsPerScreen: valuesRef.current.elementsPerScreen,
        gap,
        itemWidth,
        padding,
        paddingUnits,
        maximumItemWidth,
        total: numberOfElements,
        container: trackElementRef.current,
        outerWrapper: wrapperElementRef.current,
      };

      if (modulesRef.current.resize) {
        modulesRef.current.resize.update(options);
      } else {
        if (mode === "continuous") {
          modulesRef.current.resize = new ResizeContinuous(options);
        } else if (mode === "simple") {
          modulesRef.current.resize = new ResizeSimple(options);
        }
      }

      handleResize();
    }
  }, [
    mode,
    resizeElements,
    gap,
    itemWidth,
    padding,
    paddingUnits,
    maximumItemWidth,
    numberOfElements,
    trackElementRef.current,
    wrapperElementRef.current,
    handleResize,
  ]);

  /**
   * Init Pointer controls module
   */
  useIsomorphicLayoutEffect(() => {
    if (interactive === "always" && wrapperElementRef.current) {
      if (modulesRef.current.pointerControls) {
        modulesRef.current.pointerControls.update({
          callback: handlePointer,
        });
      } else {
        modulesRef.current.pointerControls = new PointerControls({
          outerWrapper: wrapperElementRef.current,
          callback: handlePointer,
        });
      }

      setwrapperStylesState((prevStyles) => {
        return {
          ...prevStyles,
          cursor: "grab",
          userSelect: "none",
          MozUserSelect: "none",
          WebkitTouchCallout: "none",
          touchAction: "pan-y",
        };
      });
    } else {
      if (modulesRef.current.pointerControls) {
        modulesRef.current.pointerControls.destroy();
        modulesRef.current.pointerControls = null;
      }
    }

    return () => {
      setwrapperStylesState((prevStyles) => {
        return {
          ...prevStyles,
          cursor: "auto",
          userSelect: "auto",
          MozUserSelect: "auto",
          WebkitTouchCallout: "default",
          touchAction: "auto",
        };
      });
    };
  }, [interactive, handlePointer]);

  /**
   * Init Focus module
   */
  useIsomorphicLayoutEffect(() => {
    if (interactive === "always" && wrapperElementRef.current) {
      if (modulesRef.current.focusControls) {
        modulesRef.current.focusControls.update({
          ariaLabel,
        });
      } else {
        modulesRef.current.focusControls = new FocusControls({
          id,
          container: wrapperElementRef.current,
          ariaLabel,
        });
      }
    }
  }, [id, interactive, ariaLabel]);

  /**
   * Init Motion module
   */
  useEffect(() => {
    const data = {
      position: valuesRef.current.motion.x,
      mode,
      itemWidth: valuesRef.current.carouselSizes.itemWidth,
      gapWidth: valuesRef.current.carouselSizes.gapWidth,
      totalWidth: valuesRef.current.carouselSizes.totalWidth,
      containerWidth: valuesRef.current.carouselSizes.containerWidth,
      inertionCorrection,
      inertionDecay,
      idleScroll,
      idleScrollMode,
      idleScrollDisableOnInteraction,
      isInteractedWith: valuesRef.current.isInteractedWith,
      inertionInterpolationFunction,
      motionCallback: handleMotion,
    };

    if (modulesRef.current.motion) {
      modulesRef.current.motion.update(data);
    } else {
      modulesRef.current.motion = new Motion(data);
    }
  }, [
    mode,
    valuesRef.current.carouselSizes.itemWidth,
    valuesRef.current.carouselSizes.gapWidth,
    valuesRef.current.carouselSizes.totalWidth,
    valuesRef.current.carouselSizes.containerWidth,
    inertionCorrection,
    inertionDecay,
    idleScroll,
    idleScrollDisableOnInteraction,
    inertionInterpolationFunction,
    handleMotion,
  ]);

  /**
   * Creating elements, Fake elements, Tab module
   */
  useEffect(() => {
    childrenRefs.current = [];
    animatedChildredRef.current = [];

    // const tr:React.MutableRefObject<boolean | null> = useRef(null);

    let newItems = children.map((ch, index) => {
      const ref = React.createRef<HTMLDivElement | null>();
      const compositeAnimationRef =
        React.createRef<CompositeAnimation | null>();

      childrenRefs.current.push(ref);

      animatedChildredRef.current.push({
        index,
        ref,
        fake: false,
        compositeAnimationRef,
      });

      const style: React.CSSProperties = {
        ...ch.props.style,
        width: carouselSizesState.itemWidth,
        marginRight: carouselSizesState.gapWidth,
        flexShrink: 0,
        flexGrow: 0,
      };

      return (
        <CarouselItem
          carouselItemRef={ref}
          fake={false}
          compositeAnimationRef={compositeAnimationRef}
          className={ch.props.className}
          style={style}
          key={ch.key}
        >
          {ch.props.children}
        </CarouselItem>
      );
    });

    // Creating fake elements
    const fakeItemsBefore: JSX.Element[] = [];
    const fakeItemsAfter: JSX.Element[] = [];

    for (let i = 0; i < carouselSizesState.numberOfFakeElements; i++) {
      const j = i % numberOfElements;
      const refBefore = React.createRef<HTMLDivElement>();
      const compositeAnimationRefBefore = React.createRef<CompositeAnimation>();

      fakeItemsBefore.unshift(
        <CarouselItem
          carouselItemRef={refBefore}
          fake={true}
          compositeAnimationRef={compositeAnimationRefBefore}
          className={newItems[numberOfElements - j - 1].props.className}
          style={newItems[numberOfElements - j - 1].props.style}
          key={`fake-1-${newItems[numberOfElements - j - 1].key}-${Math.floor(
            i / numberOfElements
          )}`}
        >
          {newItems[numberOfElements - j - 1].props.children}
        </CarouselItem>
      );

      animatedChildredRef.current.unshift({
        index: numberOfElements - j - 1,
        ref: refBefore,
        fake: true,
        compositeAnimationRef: compositeAnimationRefBefore,
      });

      const refAfter = React.createRef<HTMLDivElement>();
      const compositeAnimationRefAfter = React.createRef<CompositeAnimation>();

      fakeItemsAfter.push(
        <CarouselItem
          carouselItemRef={refAfter}
          fake={true}
          compositeAnimationRef={compositeAnimationRefAfter}
          className={newItems[j].props.className}
          style={newItems[j].props.style}
          key={`fake-0-${newItems[j].key}-${Math.floor(i / numberOfElements)}`}
        >
          {newItems[j].props.children}
        </CarouselItem>
      );

      animatedChildredRef.current.push({
        index: j,
        ref: refAfter,
        fake: true,
        compositeAnimationRef: compositeAnimationRefAfter,
      });
    }

    Array.prototype.unshift.apply(newItems, fakeItemsBefore);
    newItems = newItems.concat(fakeItemsAfter);

    setItems(newItems);

    // Init Tab Module

    if (interactive === "always" && wrapperElementRef.current) {
      if (modulesRef.current.tabControls) {
        modulesRef.current.tabControls.update({
          childrenRefs: childrenRefs.current,
          applySectionScrollFix,
        });
      } else {
        modulesRef.current.tabControls = new TabControls({
          id,
          container: wrapperElementRef.current,
          childrenRefs: childrenRefs.current,
          applySectionScrollFix,
        });
      }
    }
  }, [
    children,
    carouselSizesState.numberOfFakeElements,
    carouselSizesState.itemWidth,
    carouselSizesState.gapWidth,
    applySectionScrollFix,
    id,
    interactive,
    numberOfElements,
  ]);

  useIsomorphicLayoutEffect(() => {
    setPosition(true);
  });

  /**
   * Destroy all modules
   */
  useEffect(() => {
    return () => {
      if (modulesRef.current.resize) {
        modulesRef.current.resize.destroy();
        modulesRef.current.resize = null;
      }
      if (modulesRef.current.motion) {
        modulesRef.current.motion.destroy();
        modulesRef.current.motion = null;
      }
      if (modulesRef.current.focusControls) {
        modulesRef.current.focusControls.destroy();
        modulesRef.current.focusControls = null;
      }
      if (modulesRef.current.pointerControls) {
        modulesRef.current.pointerControls.destroy();
        modulesRef.current.pointerControls = null;
      }
      if (modulesRef.current.tabControls) {
        modulesRef.current.tabControls.destroy();
        modulesRef.current.tabControls = null;
      }
    }
  }, []);

  /**
   * Trigger module initial
   */
  useEffect(() => {
    if (valuesRef.current.isTriggerInitialized) {
      return;
    }

    if (
      animatedChildredRef.current.length &&
      animatedChildredRef.current.reduce((prev, cur) => {
        return (
          prev &&
          cur.ref.current !== null &&
          cur.compositeAnimationRef.current !== null
        );
      }, true)
    ) {
      valuesRef.current.isTriggerInitialized = true;

      triggerFunction({
        align: align,
        carouselPosition: valuesRef.current.position.unshifted,
        containerWidth: valuesRef.current.carouselSizes.containerWidth,
        elements: animatedChildredRef.current.map((ch, i) => {
          return {
            ref: ch.ref,
            fake: ch.fake,
            index: ch.index,
            compositeAnimationRef: ch.compositeAnimationRef,
            position:
              (i - valuesRef.current.carouselSizes.numberOfFakeElements) *
              (valuesRef.current.carouselSizes.itemWidth +
                valuesRef.current.carouselSizes.gapWidth),
          };
        }),
        gapWidth: valuesRef.current.carouselSizes.gapWidth,
        itemWidth: valuesRef.current.carouselSizes.itemWidth,
        totalWidth: valuesRef.current.carouselSizes.totalWidth,
        triggered: valuesRef.current.isTriggered,
        immidiate: true,
      });
    }
  });

  /**
   * Trigger module on trigger
   */
  useEffect(() => {
    if (triggered === valuesRef.current.isTriggered) {
      return;
    }

    valuesRef.current.isTriggered = triggered;

    triggerFunction({
      align: align,
      carouselPosition: valuesRef.current.position.unshifted,
      containerWidth: valuesRef.current.carouselSizes.containerWidth,
      elements: animatedChildredRef.current.map((ch, i) => {
        return {
          ref: ch.ref,
          fake: ch.fake,
          index: ch.index,
          compositeAnimationRef: ch.compositeAnimationRef,
          position:
            (i - valuesRef.current.carouselSizes.numberOfFakeElements) *
            (valuesRef.current.carouselSizes.itemWidth +
              valuesRef.current.carouselSizes.gapWidth),
        };
      }),
      gapWidth: valuesRef.current.carouselSizes.gapWidth,
      itemWidth: valuesRef.current.carouselSizes.itemWidth,
      totalWidth: valuesRef.current.carouselSizes.totalWidth,
      triggered: triggered,
      immidiate: false,
    });
  }, [triggered, align, triggerFunction]);

  return (
    <div
      ref={wrapperElementRef}
      className={`${className} ${styles.Carousel}`}
      style={wrapperStylesState}
    >
      <div
        ref={trackElementRef}
        className={styles.Carousel}
        style={{
          width: carouselSizesState.totalWidth
            ? carouselSizesState.totalWidth
            : "100%",
        }}
      >
        {items}
      </div>
    </div>
  );
};
