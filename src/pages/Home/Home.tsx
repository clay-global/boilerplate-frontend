import { Page } from "../interface";
import styles from "./Home.module.scss";

import { Example } from "@/entity/Example";
import { ModalControl } from "@/shared/modal";
import { Modal } from "@/feature/Modal";

export interface HomeProps {
  className?: string;
}

export const Home: Page = ({ page, data }) => {
  return (
    <>
      <Example
        page={page as Parameters<Example>[0]["page"]}
        data={data as Parameters<Example>[0]["data"]}
      />

      <ModalControl id="1" className={styles.ModalControl}>
        <b>ModalControl</b>
      </ModalControl>
      <Modal id="1" />
    </>
  );
};
