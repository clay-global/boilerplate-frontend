import dynamic from "next/dynamic";
import { getPages } from "@/api/requests/getAllPages";
import { additionalDataByType, additionalDataDefault } from "@/api/additioanalPageData";
import { PageData, AdditionalPageData, PageProps } from "@/api/interface";
import { NotFound } from "./404";
import { pagesByType, Page as PageInterface } from "@/pages/index";

interface ResponseData {
  data: PageData[];
}

interface PageComponent {
  (props: PageProps): React.ReactElement;
}

interface StaticPathParams {
  uri: string[];
}

interface StaticPath {
  params: StaticPathParams;
}

interface GetStaticPaths {
  (): Promise<{
    paths: StaticPath[];
    fallback: boolean;
  }>;
}

interface GetStaticProps {
  (options: StaticPath): Promise<{
    props: {
      page: PageData | null;
      type: string;
      data: AdditionalPageData;
    };
  }>;
}

const generateSiteMap = (pages: PageData[]) => {
  const fs = require("fs");
  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
  <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">${pages.map((page) => { return `
    <url>
      <loc>${(process.env.BASE_URL as string).replace(/\/$/, "")}${page.url}</loc>
      <lastmod>${page.last_modified}</lastmod>
      <changefreq>monthly</changefreq>
      <priority>1.0</priority>
    </url>`;
  }).join("")}
  </urlset>
`;

  fs.writeFileSync("public/sitemap.xml", sitemap);
};

export const getStaticPaths: GetStaticPaths = async () => {
  const pages = await getPages();

  generateSiteMap(pages);

  const paths: StaticPath[] = pages
    .filter((entry) => {
      return entry.uri !== "/";
    })
    .map((entry) => {
      return {
        params: {
          uri: entry.uri.split("/").splice(1),
        },
      };
    });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let urlParams = new URLSearchParams();

  const uri = "/" + params.uri.join("/");

  urlParams.append("filter[uri]", uri);
  urlParams.append("filter[published]", "true");

  try {
    const response = await fetch(
      `${process.env.API_URL}collections/pages/entries?${urlParams}`
    );
    const pageJson: ResponseData = await response.json();
    const page: PageData = pageJson.data[0];
  
    const type: string = page.blueprint.handle;
    const additionalDataRequests = {...additionalDataDefault, ...additionalDataByType[type]};
    const data: AdditionalPageData = {};
  
    for (const key of Object.keys(additionalDataRequests)) {
      data[key] = await additionalDataRequests[key]();
    }
  
    return {
      props: {
        page,
        type,
        data,
      },
    };
  } catch {
    return {
      props: {
        page: null,
        type: "404",
        data: {}
      },
    };
  }
};

export const Page: PageComponent = ({ page, type, data }) => {
  const Template: PageInterface = page ? 
    pagesByType[type] ? pagesByType[type] : NotFound: NotFound;

  if (!page) {
    return <NotFound page={null} data={null}/>;
  } else {
    return <Template page={page} data={data}></Template>;
  }
};

export default Page;
