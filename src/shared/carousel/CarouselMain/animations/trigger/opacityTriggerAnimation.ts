import { simpleTween } from "@/shared/utils/animation.utils";
import { easingTypes } from "@/app/config/ease.config";
import { AnimationItem, TriggerAnimation } from "./interface";

export const opacityTriggerAnimation: TriggerAnimation = ({
  elements,
  triggered,
}) => {
  const onTick = (element: AnimationItem, opacity: number) => {
    if (!element.compositeAnimationRef.current) return;

    element.compositeAnimationRef.current.set(`carousel-trigger`, {
      opacity: opacity,
    });
  };

  elements.forEach((el, idx) => {
    if (triggered) {
      simpleTween(
        {
          from: 0,
          to: 1,
          delay: (idx + 1) * 0.05,
          duration: 0.8,
          ease: easingTypes.easeOutPower2,
        },
        (tick) => {
          onTick(el, tick.easeProgress);
        }
      );
    } else {
      if (el.ref.current) {
        el.ref.current.style.opacity = "0";
      }
    }
  });
};
