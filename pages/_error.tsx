import { ReactElement } from "react";

interface ErrorResponse {
  statusCode: number;
}

interface Error {
  (props: { statusCode: number }): ReactElement;
  getInitialProps: (data: {
    res: ErrorResponse;
    err: ErrorResponse;
  }) => ErrorResponse;
}

const Error: Error = ({ statusCode }) => {
  return (
    <p>
      {statusCode
        ? `An error ${statusCode} occurred on server`
        : "An error occurred on client"}
    </p>
  );
};

Error.getInitialProps = ({
  res,
  err,
}: {
  res: ErrorResponse;
  err: ErrorResponse;
}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
